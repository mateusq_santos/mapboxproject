import { Component, OnInit } from '@angular/core';
import * as Mapboxgl from 'mapbox-gl';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  title = 'mapboxApplication';
  map!: Mapboxgl.Map;
  lat = -46.51744322803412;
  long = -23.674555895147677;

  ngOnInit() {
    (Mapboxgl as any).accessToken = environment.mapTokenKey;
    this.map = new Mapboxgl.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      center: [this.lat, this.long],
      zoom: 16,
    });

    this.createMarker(this.lat, this.long);
    this.map.addControl(new Mapboxgl.NavigationControl());
  }

  createMarker(lat: number, long: number) {
    new Mapboxgl.Marker({
      draggable: true,
    })
      .setLngLat([lat, long])
      .addTo(this.map);
  }
}
